package com.cleartrip.hotelsapi.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "hotelsapi")
public class HealthCheckController {
	
	private static final Log logger = LogFactory.getLog(HealthCheckController.class);

	@RequestMapping(value = "health-check")
	public String healthCheck() {
		
		logger.info("Hotels API is up");
		return "Hotels API is up";
	}
}
