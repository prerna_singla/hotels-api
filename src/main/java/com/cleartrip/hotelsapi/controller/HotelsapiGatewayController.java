package com.cleartrip.hotelsapi.controller;

import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cleartrip.hotelsapi.bean.ApiGatewayMappings;
import com.cleartrip.hotelsapi.utils.ContentRequestTransformer;
import com.cleartrip.hotelsapi.utils.HeadersRequestTransformer;
import com.cleartrip.hotelsapi.utils.URLRequestTransformer;


@RestController
@RequestMapping(value = "/hotelsapi/**", method = { GET, POST, DELETE })
public class HotelsapiGatewayController {
	private static final Logger logger = LoggerFactory.getLogger(HotelsapiGatewayController.class);
	@Autowired
	private ApiGatewayMappings apiGatewayMappings;
	private HttpClient httpClient;

	@PostConstruct
	public void init() {
		PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager();

		httpClient = HttpClients.custom().setConnectionManager(cm).build();
	}

	@RequestMapping(value = "/cross-sell/**", method = { GET, POST, DELETE })
	@ResponseBody
	public ResponseEntity<String> proxyRequest(HttpServletRequest request) {
		HttpUriRequest proxiedRequest = null;
		HttpResponse proxiedResponse = null;
		try {
			proxiedRequest = createHttpUriRequest(request);
			logger.info("request: {}", proxiedRequest);
			proxiedResponse = httpClient.execute(proxiedRequest);
			logger.info("Response {}", proxiedResponse.getStatusLine().getStatusCode());
			return new ResponseEntity<>(read(proxiedResponse.getEntity().getContent()),
					makeResponseHeaders(proxiedResponse),
					HttpStatus.valueOf(proxiedResponse.getStatusLine().getStatusCode()));
		}

		catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private HttpHeaders makeResponseHeaders(HttpResponse response) {
		HttpHeaders result = new HttpHeaders();
		Header h = response.getFirstHeader("Content-Type");
		result.set(h.getName(), h.getValue());
		return result;
	}

	private HttpUriRequest createHttpUriRequest(HttpServletRequest request)
			throws URISyntaxException, IOException, Exception {
		URLRequestTransformer urlRequestTransformer = new URLRequestTransformer(apiGatewayMappings);
		ContentRequestTransformer contentRequestTransformer = new ContentRequestTransformer();
		HeadersRequestTransformer headersRequestTransformer = new HeadersRequestTransformer();
		headersRequestTransformer.setPredecessor(contentRequestTransformer);
		contentRequestTransformer.setPredecessor(urlRequestTransformer);

		return headersRequestTransformer.transform(request).build();
	}

	private String read(InputStream input) throws IOException {
		try (BufferedReader buffer = new BufferedReader(new InputStreamReader(input))) {
			return buffer.lines().collect(Collectors.joining("\n"));
		}
	}
}
