package com.cleartrip.hotelsapi.utils;


import javax.servlet.http.HttpServletRequest;

import org.apache.http.client.methods.RequestBuilder;
import org.springframework.web.bind.annotation.RequestMethod;

import com.cleartrip.hotelsapi.bean.ApiGatewayMappings;

import java.net.URI;
import java.net.URISyntaxException;

public class URLRequestTransformer extends ProxyRequestTransformer {

	private ApiGatewayMappings apiGatewayMappings;

	public URLRequestTransformer(ApiGatewayMappings apiGatewayMappings) {
		this.apiGatewayMappings = apiGatewayMappings;
	}

	@Override
	public RequestBuilder transform(HttpServletRequest request) throws URISyntaxException, Exception {
		String requestURI = request.getRequestURI();
		URI uri;
		if (request.getQueryString() != null && !request.getQueryString().isEmpty()) {
			uri = new URI(getServiceUrl(requestURI, request) + "?" + request.getQueryString());
		} else {
			uri = new URI(getServiceUrl(requestURI, request));
		}

		RequestBuilder rb = RequestBuilder.create(request.getMethod());
		rb.setUri(uri);
		return rb;
	}

	private String getServiceUrl(String requestURI, HttpServletRequest httpServletRequest) throws Exception {

		ApiGatewayMappings.Endpoint endpoint = apiGatewayMappings.getEndpoints().stream()
				.filter(e -> requestURI.matches(e.getPath())
						&& e.getMethod() == RequestMethod.valueOf(httpServletRequest.getMethod()))
				.findFirst().orElseThrow(() -> new Exception());
		return endpoint.getLocation() + requestURI;
	}
}
