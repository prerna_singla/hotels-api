package com.cleartrip.hotelsapi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HotelsApiApplication {
	private static final Logger logger = LoggerFactory.getLogger(HotelsApiApplication.class);

	public static void main(String[] args) {
		logger.info("Hotels API Application started");
		SpringApplication.run(HotelsApiApplication.class, args);
	}
}
